package com.example.yon.wheat_rust_detection_and_diagnosis;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by yon on 3/21/18.
 */
public class SendAdapter extends RecyclerView. Adapter< SendAdapter.MyViewHolder>{

//    private ArrayList<DataModel> dataSet;
    private Context context;
    private Bitmap bitmap;
    public String wereda_name;
    private ArrayList<DataModel> dataSet;
    private ArrayList<Bitmap> bitmap_images;
    private String image_name1;
    private MaterialBetterSpinner wheat_rust_type;


    String[] SPINNERLIST = {"Huluka","Qaqaba","MW", "Ogolcho","Shorima","Dendao","Hidase","Wane"};
    String[] REGIONS = {" Arsi", "Bale", "North Shoa","West Shoa","East Shoa","East Gojam", "South Wello",
    "West Arsi","South West Shoa","Southern Tigray","South Gondar","Hadiya","West Gojam"};
    String[] WOREDAS = {"Gera", "Dedo", "Seka-Chokorsa","Omo-Beyam","Lagaynt","Aste","Farta","Smada"};


    public SendAdapter(ArrayList<Bitmap> bitmap_images,String image_name1){
//        this.context = context;
//        dataSet = data;
        this.bitmap_images = bitmap_images;
        this.image_name1 = image_name1;
        this.bitmap = bitmap;
    }
    public class MyViewHolder extends RecyclerView.ViewHolder{


        private TextView wereda,  area;

        private ImageView imagev;

        private Button btn_submit;
        private MaterialBetterSpinner wheat_rust_type,regions,woredas;

        public MyViewHolder(final View itemView) {
            super(itemView);

            imagev = (ImageView) itemView.findViewById(R.id.detailedimage);
//            wereda = (TextView) itemView.findViewById(R.id.wereda);
//            wheat_type = (TextView) itemView.findViewById(R.id.wheat_type);
            area = (TextView) itemView.findViewById(R.id.area);


            btn_submit = (Button)itemView.findViewById(R.id.btn_submit);

            wheat_rust_type = (MaterialBetterSpinner) itemView.findViewById(R.id.rust_type_spinner);
            regions = (MaterialBetterSpinner) itemView.findViewById(R.id.region_spinner);
            woredas = (MaterialBetterSpinner) itemView.findViewById(R.id.woreda_spinner);


            // notificationList.setOnContextClickListener(this);
        }


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_send_content, null);
        //ArcProgress arcProgress1 = (ArcProgress) view.findViewById(R.id.arc_progress1);

        //view.setOnClickListener(MainActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

//        Bitmap bitmap = (Bitmap) intent.getParcelableExtra("BitmapImage");
        final TextView area = holder.area;
        final ImageView imagev = holder.imagev;
        final Button btn_submit = holder.btn_submit;

        context = btn_submit.getContext();
        imagev.setImageBitmap(bitmap_images.get(0));


        final MaterialBetterSpinner wheat_rust_type = holder.wheat_rust_type;
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_dropdown_item_1line, SPINNERLIST);
        wheat_rust_type.setAdapter(arrayAdapter);

        final MaterialBetterSpinner regions = holder.regions;
        ArrayAdapter<String> regionsArrayAdapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_dropdown_item_1line, REGIONS);
        regions.setAdapter(regionsArrayAdapter);

        final MaterialBetterSpinner woredas = holder.woredas;
        ArrayAdapter<String> woredasArrayAdapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_dropdown_item_1line, WOREDAS);
        woredas.setAdapter(woredasArrayAdapter);

        wheat_rust_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//               String selected = adapterView.getItemAtPosition(i).toString();
                String selected_wheat_rust_type = wheat_rust_type.getText().toString();

                if(selected_wheat_rust_type.equals("Select Wheat Rust Type")) {

                    Toast.makeText(context,"Please Select Wheat Rust Type", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


            String area_infested = area.getText().toString();
//            String password = .getText().toString();

            if (area_infested.isEmpty() ) {
                area.setError("enter the area infested in hectare. ");

            } else {
                area.setError(null);
            }
        File wheatImageFolder = new File(Environment.getExternalStorageDirectory(),"Wheat");
        if (!wheatImageFolder.exists()) {
//            File wheatImageF = new File(Environment.getExternalStorageDirectory(),"Wheat");
            wheatImageFolder.mkdirs();
        }
        final String filename = System.currentTimeMillis() +".jpg";
//        final String filename = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".jpg";
//        wheatImageFolder.mkdirs();
        File image = new File(wheatImageFolder,filename);
        Uri uriSavedImage = Uri.fromFile(image);
//        captureImage.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);

//        File dcim = getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                inputManager.hideSoftInputFromWindow(context.getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);

                String selected_wheat_rust_type = wheat_rust_type.getText().toString();
                if(selected_wheat_rust_type.equals("Select Wheat Rust Type")) {

                    Toast.makeText(context,"Please Select Wheat Rust Type", Toast.LENGTH_LONG).show();
                }

                String selected_region = regions.getText().toString();
                String selected_woreda = woredas.getText().toString();
                String area_infested = area.getText().toString();

                Bitmap captured_image=((BitmapDrawable)imagev.getDrawable()).getBitmap();

                String string_image_name = filename;
                String date_format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
                //2018/11/16 12:08:43

//                String date_format = dateFormat.format(cal);
                Log.d("this",selected_wheat_rust_type);

//                    ByteArrayOutputStream baos=new  ByteArrayOutputStream();
//                    bitmap_image_name.compress(Bitmap.CompressFormat.PNG,100, baos);
//                    byte [] b=baos.toByteArray();
//                    String string_image_name = Base64.encodeToString(b, Base64.DEFAULT);
//Progress for submission
                final ProgressDialog progressDialog = new ProgressDialog(view.getContext(),
                        R.style.AppTheme_Dark_Dialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Sending...");
                progressDialog.show();

                new android.os.Handler().postDelayed(
                        new Runnable() {
                            public void run() {
                                progressDialog.dismiss();
                            }
                        }, 3000);

                Intent intent = new Intent(view.getContext(), HomeActivity.class);
                intent.putExtra("image", captured_image);
                intent.putExtra("image_name",string_image_name);
                intent.putExtra("wheat_type",selected_wheat_rust_type);
                intent.putExtra("region",selected_region);
                intent.putExtra("woreda",selected_woreda);
                intent.putExtra("area",area_infested);
                intent.putExtra("date",date_format);
                String dcim = Environment.getExternalStorageDirectory().toString()+"/DCIM/Camera/";
                Log.d("Path",dcim);
                File directory = new File(dcim);
                File[] files = directory.listFiles();
                String fileNameToDatabase = files[files.length -1].getName();
                for (int i= 0; i<files.length; i++){
                    Log.d("files",files[i].getName());
                }
                DatabaseHandler db = new DatabaseHandler(view.getContext());
                db.addInformation(fileNameToDatabase,selected_wheat_rust_type,Double.valueOf(area_infested)
                        ,selected_region,date_format,selected_woreda);
                view.getContext().startActivity(intent);

                List c =  db.getAllinformation();
//                List columns = db.selectColumns("image");


                Log.d("all",c.toString());
//                Log.d("image name",columns.toString());
                Log.d("count", String.valueOf(db.getInformationCount()));


// try {
//
//                    HttpClient client = new DefaultHttpClient();
//
//                    String url = "http://..";
//                    HttpPost request = new HttpPost(url);
//
//                    Log.v("send", "postURL: " + request);
//
//                    JSONObject registration = new JSONObject();
//
//                    registration.put("wheat_rust_type", selected_wheat_rust_type);
//                    registration.put("region", selected_region);
//                    registration.put("woreda", selected_woreda);
//                    registration.put("area", area_infested);
//
//                    JSONArray jsonArray = new JSONArray();
//                    jsonArray.put(registration);
//
//                    List<NameValuePair> nvps = new ArrayList <NameValuePair>();
//                    nvps.add(new BasicNameValuePair("wheatrustrequest", registration.toString()));
//                    request.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
//
//                    request.setHeader(HTTP.CONTENT_TYPE,"application/x-www-form-urlencoded;charset=UTF-8");
//                    HttpResponse response = client.execute(request);
//                    HttpEntity entity = response.getEntity();
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }catch (ClientProtocolException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } catch (Throwable t) {
//                    Toast.makeText(context, "Request failed: " + t.toString(),
//                            Toast.LENGTH_LONG).show();
//
//
//                }


//                 wereda_name = wereda.getText().toString();

//                MaterialBetterSpinner wheat_rust_type = holder.wheat_rust_type;
//                Log.d("values",wereda_name);


            }
        });
//        imagev.setImageBitmap(bitmap);





    }

    @Override
    public int getItemCount() {
        return bitmap_images.size();
    }

//    @Override
//    public int getItemCount() {
//        return 0;
//    }


}
