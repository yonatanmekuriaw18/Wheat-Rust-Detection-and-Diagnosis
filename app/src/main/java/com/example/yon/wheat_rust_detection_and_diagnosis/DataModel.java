package com.example.yon.wheat_rust_detection_and_diagnosis;

/**
 * Created by yon on 3/9/18.
 */
public class DataModel {

    String name;
    String version;
    String version2;
    double performance;
    String username;
    String position;
    int id_;
    int image;

    public DataModel(){

    }

    public DataModel(String name, String version, int id_, int image) {
        this.name = name;
        this.version = version;
        this.id_ = id_;
        this.image=image;
    }

    public DataModel(String name, String version, int id_) {
        this.name = name;
        this.version = version;
        this.id_ = id_;
        this.image=image;
    }
    public DataModel(String name, String version,String version2, int id_, int image) {
        this.name = name;
        this.version = version;
        this.version2 = version2;
        this.id_ = id_;
        this.image=image;
    }
    public DataModel(String name, String version,String version2, int id_) {
        this.name = name;
        this.version = version;
        this.version2 = version2;
        this.id_ = id_;
        this.image=image;
    }
    public DataModel(String name, String version,String version2,double performance, int id_, int image) {
        this.name = name;
        this.version = version;
        this.version2 = version2;
        this.performance = performance;
        this.id_ = id_;
        this.image=image;
    }
    public DataModel(String name, String version,String version2,double performance,String username,String position, int id_, int image) {
        this.name = name;
        this.version = version;
        this.version2 = version2;
        this.performance = performance;
        this.username = username;
        this.position = position;
        this.id_ = id_;
        this.image=image;
    }


    public String getName() {
        return name;
    }


    public String getVersion() {
        return version;
    }
    public String getVersion2() {
        return version2;
    }
    public String getPosition() {
        return position;
    }

    public String getUsername() {
        return username;
    }
    public Double getPerformance(){
        return  performance;
    }


    public int getImage() {
        return image;
    }

    public int getId() {
        return id_;
    }
}
