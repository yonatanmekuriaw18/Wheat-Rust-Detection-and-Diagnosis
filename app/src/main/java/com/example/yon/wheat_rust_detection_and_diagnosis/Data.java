package com.example.yon.wheat_rust_detection_and_diagnosis;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by yon on 3/9/18.
 */
public class Data {




    static DateFormat df = new SimpleDateFormat("dd/MM/yy ");
    static Date dateobje = new Date();
    static String dateprint  = df.format(dateobje);
    static String[] nameArray = {"Yellow Rust ", "Yellow Rust", "Yellow Rust",
            "Yellow Rust", "Yellow Rust", "Yellow Rust", "Yellow Rust",
            "Yellow Rust", "Yellow Rust", "Yellow Rust", "Yellow Rust"};
    static String[] nameArray1 = {"የተገልጋይ እርካታን ማሳደግ ", "የተማሪዎች/የ ሰልጣኞች ተወዳዳሪነትን ማሳደግ", "የተማሪዎችን/ የሰልጣኞችን ስነምግባር ማሻሻል",
            "የሀብት አጠቃቀም ውጤታማነትን ማሻሻል", "የሀብት ምንጮችን ማሳደግ", "የትምህርትና ስልጠና ጥራት ማሻሻል", "የትምህርትና ስልጠና ተገቢነትን ማሻሻል",
            "የባለድርሻ አካላት አጋርነትን ማሻሻል", "የትምህርትና ስልጠና ድጋፍ፣ ክትትልና መረጃ ስርአትን ማሻሻል", "የትምህርትና ስልጠና አገልግሎት ፍትሀዊ ተደራሽነትን ማሳደግ", "የትምህርትና ስልጠና ዘርፉን ውስጣዊ ብቃት ማሳደግ"};
    static String[] versionArray = {"1.5", "1.6", "2.0-2.1", "2.2-2.2.3", "2.3-2.3.7", "3.0-3.2.6", "4.0-4.0.4", "4.1-4.3.1", "4.4-4.4.4", "5.0-5.1.1","6.0-6.0.1"};
    static String[] versionArray2 = {dateprint, dateprint, dateprint, dateprint, dateprint, dateprint, dateprint, dateprint, dateprint, dateprint,dateprint};

    static Double[] userPerformanceArray = {50.0, 20.0, 30.0, 90.0, 10.0, 60.0, 66.5, 55.0, 32.0, 70.5,62.3};

    static String[] userName = {"Dereje Abebe","Abebe Kebede","Muhammed Kemal","Alazar Alemayehu","Solomon Abriham",
            "Solomon Abriham","Feysal Mama","Yonatan Mekuriaw","Dereje Abebe","Dereje Abebe","Yadu Guluma"};
    static String[] org_structure = {"የከፍተኛ ትምህርት አሰተዳደር ጉዳዮች ጀነራል ዳይሬክቶሬት","የከፍተኛ ትምህርት የምርምርና አካዳሚክ ጉዳዮች ጀኔራል ዳይሬክቶሬት","የከፍተኛ ትምህርት ኢንስፔክሽን ዳይሬክቶሬት","የከፍተኛ ትምህርት አግባብነትና ጥራት ኤጀንሲ","የትምህርት ስትራቴጂክ ማዕከል",
            "የኢትዮጵያ ኢጁኬሽናል ሪሰርች ኔትወርክ ዳታ ሴንተር","የኢትዮጵያ ኢጁኬሽናል ሪሰርች ኔትወርክ ዳታ ሴንተር","የኢትዮጵያ ኢጁኬሽናል ሪሰርች ኔትወርክ ዳታ ሴንተር","የከፍተኛ ትምህርት የምርምርና አካዳሚክ ጉዳዮች ጀኔራል ዳይሬክቶሬት","የከፍተኛ ትምህርት የምርምርና አካዳሚክ ጉዳዮች ጀኔራል ዳይሬክቶሬት","የከፍተኛ ትምህርት የምርምርና አካዳሚክ ጉዳዮች ጀኔራል ዳይሬክቶሬት"};
    static String[] position = {"System Team Leader","Network Engineer","Application Developer","Software Engineer","Application Designer","System Team Leader","System Team Leader",
            "System Team Leader","System Team Leader","System Team Leader","System Team Leader"};
    static Integer[] drawableArray = {R.drawable.moelogo, R.drawable.moelogo, R.drawable.moelogo,
            R.drawable.moelogo, R.drawable.moelogo, R.drawable.moelogo, R.drawable.moelogo,
            R.drawable.moelogo, R.drawable.moelogo, R.drawable.moelogo,R.drawable.moelogo};
    static Integer[] drawableArray2 = {R.drawable.moelogo
            , R.drawable.moelogo, R.drawable.moelogo,
            R.drawable.moelogo, R.drawable.moelogo, R.drawable.moelogo, R.drawable.moelogo,
            R.drawable.moelogo, R.drawable.moelogo, R.drawable.moelogo,R.drawable.moelogo};

    static Integer[] id_ = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};




    public static ArrayList<DataModel> notificationList() {

        ArrayList<DataModel> data = new ArrayList<DataModel>();
        for (int i = 0; i < Data.nameArray.length; i++) {
            data.add(new DataModel(
                    Data.nameArray[i],
                    Data.versionArray[i],
                    Data.versionArray2[i],
                    Data.id_[i],
                    Data.drawableArray2[i]
            ));
        }

        return data;

    }
    public static ArrayList<DataModel> notificationsList() {

        ArrayList<DataModel> data = new ArrayList<DataModel>();
        for (int i = 0; i < Data.nameArray.length; i++) {
            data.add(new DataModel(
                    Data.nameArray[i],
                    Data.versionArray[i],
                    Data.versionArray2[i],
                    Data.id_[i]
            ));
        }

        return data;

    }
    public static ArrayList<DataModel> approveList() {

        ArrayList<DataModel> data = new ArrayList<DataModel>();
        for (int i = 0; i < Data.nameArray.length; i++) {
            data.add(new DataModel(
                    Data.nameArray[i],
                    Data.versionArray[i],
                    Data.versionArray2[i],
                    Data.id_[i]
            ));
        }

        return data;

    }
}
