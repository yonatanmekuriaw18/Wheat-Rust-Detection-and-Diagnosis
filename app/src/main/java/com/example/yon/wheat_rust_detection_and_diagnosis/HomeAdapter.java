package com.example.yon.wheat_rust_detection_and_diagnosis;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by yon on 9/27/18.
 */
public class  HomeAdapter extends RecyclerView. Adapter< HomeAdapter.MyViewHolder>{
    private ArrayList<DataModel> dataSet;
    private Context mContext;
    private ArrayList<String> bitmap_images;
    private String image_name;
    private ArrayList<DataModel> data;
    private ArrayList<String> info;
    private ArrayList<String> info2;
    private String wheat_type;
    private String region;
    private String woreda;
    private String date;
    private Uri uriSavedImage;
    // public OnItemClickListener mItemClickListener;

    public HomeAdapter(ArrayList<String> bitmap_images, String image_name,String wheat_type, String region,String woreda,String date){
//        this.context = context;
//        dataSet = data;
        this.bitmap_images = bitmap_images;
        this.image_name = image_name;
        this.wheat_type = wheat_type;
        this.woreda = woreda;
        this.date = date;
    }
    public HomeAdapter(ArrayList<String> info,ArrayList<String> info2,Uri uriSavedImage){
//        this.context = context;
        this.info = info;
        this.info2 = info2;
        this.uriSavedImage=uriSavedImage;
        this.bitmap_images = bitmap_images;
        this.image_name = image_name;
        this.wheat_type = wheat_type;
        this.woreda = woreda;
        this.date = date;
    }
// public HomeAdapter(ArrayList<DataModel> data){
////        this.context = context;
//        this.info = info;
//        dataSet=data;
//        this.bitmap_images = bitmap_images;
//        this.image_name = image_name;
//        this.wheat_type = wheat_type;
//        this.woreda = woreda;
//        this.date = date;
//    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView mTitleText, mDateAndTimeText, mRepeatInfoText,mDateText;
        private ImageView mActiveImage , mThumbnailImage;
        private HomeAdapter mAdapter;
        private TextDrawable mDrawableBuilder;
        private RelativeLayout notificationList;
        private ImageButton cardPopupMenu;


        public MyViewHolder(final View itemView) {
            super(itemView);

            notificationList = (RelativeLayout)itemView.findViewById(R.id.approves);
            mTitleText = (TextView) itemView.findViewById(R.id.approve_title);
            mDateText = (TextView) itemView.findViewById(R.id.date_time);
            mThumbnailImage = (ImageView) itemView.findViewById(R.id.approve_image);
            cardPopupMenu = (ImageButton)itemView.findViewById(R.id.card_popup_menu);
            // notificationList.setOnContextClickListener(this);
        }
        //        @Override
//        public void onClick(View v) {
//            if (mItemClickListener != null) {
//                mItemClickListener.onItemClick(itemView, getPosition());
//            }
//        }
        public void setReminderTitle(String title) {

            //mTitleText.setText(title);
            String letter = "A";

            if(title != null && !title.isEmpty()) {
                letter = title.substring(0, 1);
            }

            //int color = mColorGenerator.getRandomColor();
            int color = ColorGenerator.MATERIAL.getRandomColor();
            // Create a circular icon consisting of  a random background colour and first letter of title
            mDrawableBuilder = TextDrawable.builder()
                    .buildRound(letter,color );
//            mThumbnailImage.setImageDrawable(R.drawable.moelogo);
//
        }
//        @Override
//        public void onClick(View v) {
//            if (mItemClickListener != null) {
//                mItemClickListener.onItemClick(itemView, getPosition());
//            }
//        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_home_content, null);
        //ArcProgress arcProgress1 = (ArcProgress) view.findViewById(R.id.arc_progress1);

        //view.setOnClickListener(MainActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        RelativeLayout notificationList = holder.notificationList;
        ImageView mThumbnailImage = holder.mThumbnailImage;
        TextView mTitleText = holder.mTitleText;
        TextView mDateText = holder.mDateText;

        DatabaseHandler db = new DatabaseHandler(mTitleText.getContext());

        List c =  db.getAllinformation();
//      List columns = db.selectColumns("image");
        ArrayList<String > imagenamelist = new ArrayList <String>();

        for (int i =1;i<c.size();i+=7){
            Object imagename = c.get(i);
//            i+=7;
            imagenamelist.add(imagename.toString());
        }
        ArrayList<String > rusttypelist = new ArrayList<String>();
        for (int i =2;i<c.size();i+=8){
            Object rust_type = c.get(i);
//            i+=7;
            rusttypelist.add(rust_type.toString());
        }
        ArrayList<String > date = new ArrayList<String>();
        for (int i =5;i<c.size();i+=11){
            Object dateo = c.get(i);
//            i+=7;
            date.add(dateo.toString());
        }
        List name = db.selectColumnsname();
        Collections.reverse(name);
        List type = db.selectColumnrusttype();
        Collections.reverse(type);
        List datee = db.selectColumnsdate();
        Collections.reverse(datee);

        ImageButton cardPopupMenu = holder.cardPopupMenu;
        //holder.setReminderTitle(dataSet.get(listPosition).getName());
//        mTitleText.setText(imagenamelist.get(listPosition).getName());
//          String title = "ማ";
        mTitleText.setText(type.get(listPosition).toString());
        mDateText.setText(datee.get(listPosition).toString());

//        holder.setReminderTitle(title);

        String filename = "";
//        String url = "file://"+ Environment.getExternalStorageDirectory()+"/Wheat/"+name.get(listPosition).toString();
        String url = Environment.getExternalStorageDirectory().getAbsolutePath() +"/DCIM/Camera/"+ name.get(listPosition).toString();
//        Bitmap images_display = BitmapFactory.decodeFile(url);

        BitmapFactory.Options o = new BitmapFactory.Options();
//        o.inPurgeable = true;
        o.inSampleSize = 8;

        Bitmap images_display = BitmapFactory.decodeFile(url, o);

        Log.d("url",url);
//        mThumbnailImage.setImageResource(R.drawable.leafrust);
        mThumbnailImage.setImageBitmap(images_display);

//        DatabaseHandler db = new DatabaseHandler(mTitleText.getContext());
//        List c =  db.getAllinformation();

//        holder.notificationList.setOnClickListener(onClickListener(listPosition));
        cardPopupMenu.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
//                Toast.makeText(mContext, "", Toast.LENGTH_SHORT).show();

            }
        });
        notificationList.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {


                //int position = (int)v.getTag();
                //itemView.getContext().startActivity(new Intent(itemView.getContext(),LoginActivity.class));
                // TextView mTitleText = holder.mTitleText;
                Log.d("position",String.valueOf(listPosition));

                Toast.makeText(mContext, "The Image is Not sent to the Server", Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(v.getContext(),R.style.AppCompatAlertDialogStyle);



                builder.setTitle("Image Not Sent");
                builder.setMessage("Image Either not Sent to the Server or Result it Not Available");

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface alert, int which) {
                        // TODO Auto-generated method stub
                        //Do something

//                        Intent intent = new Intent(v.getApplicationContext(),ApproveActivity.class);
//                        v.getContext().startActivity(intent);
                        alert.dismiss();
                    }
                });

                builder.show();
//                Intent i = new Intent(v.getContext(),DetailedActivity.class);
////
//                String title =  dataSet.get(listPosition).getName();
////
//                i.putExtra("position",listPosition);
////
//                i.putExtra("title",title);
//
////                i.putExtra("")
//                v.getContext().startActivity(i);

//                AlertDialog.Builder builder =
//                        new AlertDialog.Builder(v.getContext(),R.style.AppCompatAlertDialogStyle);

                // set dialog title & message, and provide Button to dismiss

//                builder.setTitle("ማጽደቅ");
//                builder.setMessage("ለማጽደቅ እርግጠኛ ነዎት?");
//
//                builder.setPositiveButton("አዎ", new DialogInterface.OnClickListener() {
//
//                    @Override
//                    public void onClick(DialogInterface alert, int which) {
//                        // TODO Auto-generated method stub
//                        //Do something
//
////                        Intent intent = new Intent(v.getApplicationContext(),ApproveActivity.class);
////                        v.getContext().startActivity(intent);
//                        alert.dismiss();
//                    }
//                });
//
//                builder.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return info.size();
    }

//    private View.OnClickListener onClickListener(final int position) {
//        return new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                Dialog dialog = new Dialog();
////               // dialog.setContentView(R.layout.item_recycler);
////                dialog.setTitle("Position " + position);
////                dialog.setCancelable(true); // dismiss when touching outside Dialog
////
////                // set the custom dialog components - texts and image
////
////
////                dialog.show();
//                Log.d("position",String.valueOf(position));
//                Intent ic = new Intent(mContext,LoginActivity.class);
//                mContext.startActivity(ic);
//
//            }
//        };
//    }



}



