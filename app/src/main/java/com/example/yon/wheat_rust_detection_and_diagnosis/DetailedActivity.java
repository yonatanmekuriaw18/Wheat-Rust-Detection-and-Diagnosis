package com.example.yon.wheat_rust_detection_and_diagnosis;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;

public class DetailedActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private static RecyclerView.Adapter adapter;

    private NavigationView navigationView;
    private DetailAdapter DetailAdapter;

    private DataModel dataModel;

    private static final int REQUEST_PHOTO = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed);
//        ImageView imagev = (ImageView)findViewById(R.id.detailedimage);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_detailed);

        setSupportActionBar(toolbar);

        Bundle bundle = getIntent().getExtras();
        int position = bundle.getInt("position");

//        Intent intent = getIntent();
//        Bitmap bitmap = (Bitmap) intent.getParcelableExtra("BitmapImage");
//        imagev.setImageBitmap(bitmap);
//        if (position == 0) {
//              imagev.setImageResource(R.drawable.leafrust);
//

//
//        }
//        else if(position == 1){
//
//            imagev.setImageResource(Data.drawableArray2[1]);
//        }
//        else if(position == 2){
//
//            imagev.setImageResource(Data.drawableArray2[2]);
//        }
//        else if(position == 3){
//
//            imagev.setImageResource(Data.drawableArray2[3]);
//        }
//        else if(position == 4){
//
//
//            imagev.setImageResource(Data.drawableArray2[4]);
//        }
//        else if(position == 5){
//
//
//            imagev.setImageResource(Data.drawableArray2[5]);
//        }
//        else if(position == 6){
//
//            imagev.setImageResource(Data.drawableArray2[6]);
//        }
//
//        else if(position == 7){
//
//            imagev.setImageResource(Data.drawableArray2[7]);
//        }
//        else if(position == 8){
//
//            imagev.setImageResource(Data.drawableArray2[8]);
//        }
//        else if(position == 9){
//
//            imagev.setImageResource(Data.drawableArray2[9]);
//        }
//        else{
//            imagev.setImageResource(Data.drawableArray2[9]);
//
//        }
//

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_detailed);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.home_detailed);
//        rView.setLayoutManager(lLayout);
        recyclerView.setHasFixedSize(true);
//
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        // List<DataModel> allItems = new ArrayList<DataModel>();

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        ArrayList<DataModel> data = Data.notificationsList();
        //removedItems = new ArrayList<Integer>();

//        adapter = new DetailAdapter(data);
//        recyclerView.setAdapter(adapter);
//

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.capture);
        final Intent captureImage = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(captureImage, REQUEST_PHOTO);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_detailed);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_bar_location) {
//            AlertDialog.Builder builder =
//                    new AlertDialog.Builder(DetailedActivity.this,R.style.AppCompatAlertDialogStyle);
//
//            builder.setTitle("Location");
//
//            builder.setMessage("Arada, Addis Ababa");
//
//            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//
//                @Override
//                public void onClick(DialogInterface alert, int which) {
//                    // TODO Auto-generated method stub
//                    alert.dismiss();
//                }
//            });
//
//            builder.show();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_announcement) {
//            Intent ic = new Intent(getApplicationContext(),NotificationActivity.class);
//            startActivity(ic);
        } else if (id == R.id.nav_cascade) {
//            Intent i = new Intent(getApplicationContext(),NotifyActivity.class);
//            startActivity(i);

        }
//        else if (id == R.id.nav_evaluation) {
//            Intent ic = new Intent(getApplicationContext(),NotifyActivity.class);
//            startActivity(ic);
//
//        } else if (id == R.id.nav_report) {
//            Intent ui = new Intent(getApplicationContext(),UsersPerformanceActivity.class);
//            startActivity(ui);
//        }
        else if (id == R.id.nav_profile) {
//            Intent ca = new Intent(getApplicationContext(),SignupActivity.class);
//            startActivity(ca);
        }
        else if (id == R.id.nav_logout) {
//            Intent ic = new Intent(getApplicationContext(),LoginActivity.class);
//            startActivity(ic);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_detailed);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }
}
