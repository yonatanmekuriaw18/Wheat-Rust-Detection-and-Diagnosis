package com.example.yon.wheat_rust_detection_and_diagnosis;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;

/**
 * Created by yon on 9/27/18.
 */
public class  DetailAdapter extends RecyclerView. Adapter< DetailAdapter.MyViewHolder>{
    private ArrayList<DataModel> dataSet;
    private Context mContext;
    private ImageView mThumbnailImage;

    // public OnItemClickListener mItemClickListener;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private ImageView mThumbnailImage;

        public MyViewHolder(final View itemView) {
            super(itemView);
            mThumbnailImage = (ImageView) itemView.findViewById(R.id.imageview_detailed);
//            mThumbnailImage.setImageResource(R.drawable.leafrust);
        }

    }

    public  DetailAdapter(ArrayList<DataModel> data) {
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {




        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_detailed_content, null);
        //ArcProgress arcProgress1 = (ArcProgress) view.findViewById(R.id.arc_progress1);

        //view.setOnClickListener(MainActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


        ImageView mThumbnailImage = holder.mThumbnailImage;
        mThumbnailImage.setImageResource(R.drawable.leafrust);


        Log.d("position",String.valueOf(listPosition));
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }




}



