package com.example.yon.wheat_rust_detection_and_diagnosis;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yon on 3/22/18.
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "WheatRust";
    private static final String TABLE_INFORMATION = "RustInformation";
    private static final String KEY_ID = "id";
    private static final String KEY_IMAGE_NAME = "image";
    private static final String KEY_WHEAT_TYPE = "wheat";
    private static final String KEY_AREA = "area";
    private static final String KEY_REGION = "region";
    private static final String KEY_DATE = "date";
    private static final String KEY_WOREDA = "woreda";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_INFORMATION_TABLE = "CREATE TABLE " + TABLE_INFORMATION  + "("
                + KEY_ID  + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + KEY_IMAGE_NAME + " TEXT, "
                + KEY_WHEAT_TYPE + " TEXT, "
                + KEY_AREA + " REAL, "
                + KEY_REGION + " TEXT, "
                + KEY_DATE + " TEXT, "
                + KEY_WOREDA + " TEXT )";
        db.execSQL(CREATE_INFORMATION_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INFORMATION);

        // Create tables again
        onCreate(db);
    }

    // code to add the new information
    void addInformation(String image_name,String wheat_type,Double area,String region,String date,String woreda) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_IMAGE_NAME, image_name); // me
        values.put(KEY_WHEAT_TYPE, wheat_type); // me
        values.put(KEY_AREA, area); //
        values.put(KEY_REGION,region);
        values.put(KEY_DATE,date);
        values.put(KEY_WOREDA,woreda);

        // Inserting Row
        db.insert(TABLE_INFORMATION, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    public ArrayList<String> searchImageByName(String name){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from RustInformation where image='"+name+"'", null );

        if(res!=null){
            res.moveToFirst();

        }
        ArrayList<String> informations = new ArrayList<String>();

        String image_name = res.getString(res.getColumnIndex(KEY_IMAGE_NAME));
        String wheat_type = res.getString(res.getColumnIndex(KEY_WHEAT_TYPE));
        String region = res.getString(res.getColumnIndex(KEY_REGION));
        String woreda = res.getString(res.getColumnIndex(KEY_WOREDA));
        String date = res.getString(res.getColumnIndex(KEY_DATE));
//        Double area = res.getDouble(res.getColumnIndex(KEY_AREA));
        String area = String.valueOf(res.getColumnIndex(KEY_AREA));

        informations.add(image_name);
        informations.add(wheat_type);
        informations.add(area);
        informations.add(region);
        informations.add(date);
        informations.add(woreda);



        return  informations;
    }
    public List<String> selectColumnsname(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select image from RustInformation", null );

        if(res!=null){
            res.moveToFirst();

        }
        List<String> informations = new ArrayList<String>();

        if (res.moveToFirst()) {
            do {
            String image_name = res.getString(0);
            informations.add(image_name);
            } while (res.moveToNext());
        }




        return  informations;
    }
    public List<String> selectColumnsdate(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select date from RustInformation", null );

        if(res!=null){
            res.moveToFirst();

        }
        List<String> informations = new ArrayList<String>();

        if (res.moveToFirst()) {
            do {
            String image_name = res.getString(0);
            informations.add(image_name);
            } while (res.moveToNext());
        }



        return  informations;
    }
public List<String> selectColumnrusttype(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select wheat from RustInformation", null );

        if(res!=null){
            res.moveToFirst();

        }
        List<String> informations = new ArrayList<String>();

        if (res.moveToFirst()) {
            do {
            String image_name = res.getString(0);
            informations.add(image_name);
            } while (res.moveToNext());
        }



        return  informations;
    }

    public boolean Exists(String imageName) {


        SQLiteDatabase db = this.getWritableDatabase();
        // Cursor cursor = db.rawQuery(selectQuery, null);
        Cursor cursor =  db.rawQuery( "select * from RustInformation where name='"+imageName+"'", null );


        //Cursor cursor = db.query(TABLE_NAME, columns, selection, selectionArgs, null, null, null, limit);
        boolean exists = (cursor.getCount() > 0);
        cursor.close();
        return exists;
    }

    public List<String> getAllinformation() {
        List<String> informationList = new ArrayList<String>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_INFORMATION;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {


                Integer id = cursor.getInt(0);
                String image_name = cursor.getString(1);
                String wheat_type = cursor.getString(2);
                Double area = cursor.getDouble(3);
                String region = cursor.getString(4);
                String date = cursor.getString(5);
                String woreda = cursor.getString(6);
                // Adding contact to list
                informationList.add(String.valueOf(id));
                informationList.add(image_name);
                informationList.add(wheat_type);
                informationList.add(String.valueOf(area));
                informationList.add(region);
                informationList.add(date);
                informationList.add(woreda);


            } while (cursor.moveToNext());
        }

        return informationList;
    }

    public int updateImageInformation(String status,String imageName) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_IMAGE_NAME,imageName);
        // updating row
        /*return db.update(TABLE_CONTACTS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });*/
        /*return db.update(TABLE_CONTACTS, values, KEY_NAME + " = ?",
                new String[] { CompanyActivity.copyText });*/
        return db.update(TABLE_INFORMATION,values,KEY_IMAGE_NAME + " = '" + imageName + "'",null);
    }


    public void deleteInformation(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_INFORMATION, KEY_ID + " = ?",
                new String[] { String.valueOf(id) });
        db.close();
    }

    public int getInformationCount() {
        String countQuery = "SELECT  * FROM " + TABLE_INFORMATION;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
//        cursor.close();


        return cursor.getCount();
    }
}

