package com.example.yon.wheat_rust_detection_and_diagnosis;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    private static RecyclerView.Adapter adapter;
    //    @Bind(R.id.link_login) TextView _loginLink;
    private NavigationView navigationView;
    private HomeAdapter HomeAdapter;
    //    private ImageView imageHolder;
    private DataModel dataModel;
    private Uri mFileUri;
    public static String copyText;
    public ArrayList<String> imagenamelist;
    public ArrayList<String> rusttypelist;
    //    private final int requestCode = 20;
    public ArrayList<String> dates;
    private static final int REQUEST_PHOTO = 2;
    public String filename;
    public Uri uriSavedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        imageHolder = (ImageView)findViewById(R.id.imageviewcaptured);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.home);
//        rView.setLayoutManager(lLayout);
//        recyclerView.setHasFixedSize(true);
//
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        // List<DataModel> allItems = new ArrayList<DataModel>();

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //removedItems = new ArrayList<Integer>();

        DatabaseHandler db = new DatabaseHandler(this);
        List c =  db.getAllinformation();
//                List columns = db.selectColumns("image");
        imagenamelist = new ArrayList <String>();


        for (int i =1;i<c.size();i+=7){
            Object imagename = c.get(i);
//            i+=7;
            imagenamelist.add(imagename.toString());
        }
        rusttypelist = new ArrayList<String>();
        for (int i =2;i<c.size();i+=8){
            Object rust_type = c.get(i);
//            i+=7;
            rusttypelist.add(rust_type.toString());
        }
        dates = new ArrayList<String>();
        for (int i =5;i<c.size();i+=11){
            Object dateo = c.get(i);
//            i+=7;
            dates.add(dateo.toString());
        }

        Log.d("image name",imagenamelist.toString());

        Log.d("all",c.toString());
        Log.d("date",dates.toString());
        Log.d("imagelist",imagenamelist.toString());
        Log.d("wheat",rusttypelist.toString());



        //Save file to specific folder
        File wheatImageFolder = new File(Environment.getExternalStorageDirectory(),"Wheat");
        if (!wheatImageFolder.exists()) {
//            File wheatImageF = new File(Environment.getExternalStorageDirectory(),"Wheat");
            wheatImageFolder.mkdirs();
        }
        Random generator = new Random();
        int n = 100000;
        n = generator.nextInt(n);
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        filename =  timeStamp+".jpg";
//        wheatImageFolder.mkdirs();
        File image = new File(wheatImageFolder,filename);
        uriSavedImage = Uri.fromFile(image);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.capture);
        final Intent captureImage = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        ArrayList<DataModel> data = Data.notificationsList();
        ArrayList<String> info = informationList();
        ArrayList<String> info2 = informationList2();
        adapter = new HomeAdapter(info,info2,uriSavedImage);
        recyclerView.setAdapter(adapter);
//        captureImage.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                captureImage.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
                startActivityForResult(captureImage, REQUEST_PHOTO);

//                DatabaseHandler db = new DatabaseHandler(view.getContext());
//                List c =  db.getAllinformation();

//                Log.d("values",c.toString());
            }
        });

        List name = db.selectColumnsname();
        Collections.reverse(name);
        List type = db.selectColumnrusttype();
        Collections.reverse(type);
        List date = db.selectColumnsdate();
        Collections.reverse(date);

        Log.d("type",type.toString());
        Log.d("name",name.toString());
        Log.d("date",date.toString());
    }
    public ArrayList<String> informationList() {
        DatabaseHandler db = new DatabaseHandler(this);
        List c =  db.getAllinformation();
        imagenamelist = new ArrayList <String>();
        List name = db.selectColumnsname();
        Collections.reverse(name);
        List type = db.selectColumnrusttype();
        Collections.reverse(type);
        List date = db.selectColumnsdate();
        Collections.reverse(date);


        for (int i =1;i<c.size();i+=7){
            Object imagename = c.get(i);
//            i+=7;
            imagenamelist.add(imagename.toString());
        }
        rusttypelist = new ArrayList<String>();
        for (int j =2;j<c.size();j+=8){
            Object rust_type = c.get(j);
//            i+=7;
            rusttypelist.add(rust_type.toString());
        }
        dates = new ArrayList<String>();
        for (int k =5;k<c.size();k+=11){
            Object dateo = c.get(k);
//            i+=7;
            dates.add(dateo.toString());
        }


        ArrayList<String> info = new ArrayList<String>();
        for (int i = 0; i < date.size() ; i++) {
            info.add(name.get(i).toString());
//            info.add(type.get(i).toString());
//            info.add(date.get(i).toString());
        }

        return info;

    }
    public ArrayList<String> informationList2() {
        DatabaseHandler db = new DatabaseHandler(this);
        List c =  db.getAllinformation();
        imagenamelist = new ArrayList <String>();
        List name = db.selectColumnsname();
        Collections.reverse(name);
        List type = db.selectColumnrusttype();
        Collections.reverse(type);
        List date = db.selectColumnsdate();
        Collections.reverse(date);


        for (int i =1;i<c.size();i+=7){
            Object imagename = c.get(i);
//            i+=7;
            imagenamelist.add(imagename.toString());
        }
        rusttypelist = new ArrayList<String>();
        for (int j =2;j<c.size();j+=8){
            Object rust_type = c.get(j);
//            i+=7;
            rusttypelist.add(rust_type.toString());
        }
        dates = new ArrayList<String>();
        for (int k =5;k<c.size();k+=11){
            Object dateo = c.get(k);
//            i+=7;
            dates.add(dateo.toString());
        }


        ArrayList<String> info = new ArrayList<String>();
        for (int i = 0; i < date.size() ; i++) {
//            info.add(name.get(i).toString());
            info.add(type.get(i).toString());
//            info.add(date.get(i).toString());
        }

        return info;

    }
    public ArrayList<String> informationList3() {
        DatabaseHandler db = new DatabaseHandler(this);
        List c =  db.getAllinformation();
        imagenamelist = new ArrayList <String>();
        List name = db.selectColumnsname();
        Collections.reverse(name);
        List type = db.selectColumnrusttype();
        Collections.reverse(type);
        List date = db.selectColumnsdate();
        Collections.reverse(date);


        for (int i =1;i<c.size();i+=7){
            Object imagename = c.get(i);
//            i+=7;
            imagenamelist.add(imagename.toString());
        }
        rusttypelist = new ArrayList<String>();
        for (int j =2;j<c.size();j+=8){
            Object rust_type = c.get(j);
//            i+=7;
            rusttypelist.add(rust_type.toString());
        }
        dates = new ArrayList<String>();
        for (int k =5;k<c.size();k+=11){
            Object dateo = c.get(k);
//            i+=7;
            dates.add(dateo.toString());
        }


        ArrayList<String> info = new ArrayList<String>();
        for (int i = 0; i < date.size() ; i++) {
            info.add(name.get(i).toString());
//            info.add(type.get(i).toString());
//            info.add(date.get(i).toString());
        }

        return info;

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_bar_location) {
//            AlertDialog.Builder builder =
//                    new AlertDialog.Builder(HomeActivity.this,R.style.AppCompatAlertDialogStyle);
//
//            builder.setTitle("Location");
//
//            builder.setMessage("Arada, Addis Ababa");
//
//            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//
//                @Override
//                public void onClick(DialogInterface alert, int which) {
//                    // TODO Auto-generated method stub
//                    alert.dismiss();
//                }
//            });
//
//            builder.show();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_PHOTO && resultCode == RESULT_OK){
//            Bitmap bitmap = (BitmaDp)data.getExtras().get("data");
//            Uri selectedImage = data.getData();
            Bundle extras = data.getExtras();
            Bitmap photo = (Bitmap) extras.get("data");
//            Bitmap photo = BitmapFactory.decodeFile(filename);

//            imageHolder.setImageBitmap(photo);
            //       imageHolder.setImageURI(selectedImage);
//            Intent intent = new Intent(this, DetailedActivity.class);
//            intent.putExtra("BitmapImage", bitmap);
//            imageHolder.setImageBitmap(bitmap);
//            String mFilepath = mFileUri.toString();
//            if(mFilepath !=null)
//            {

            Intent intent = new Intent(this, SendActivity.class);
            intent.putExtra("BitmapImage",photo);
            startActivity(intent);
//            }


        }
    }
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_announcement) {
//            Intent ic = new Intent(getApplicationContext(),NotificationActivity.class);
//            startActivity(ic);
        } else if (id == R.id.nav_cascade) {
//            Intent i = new Intent(getApplicationContext(),NotifyActivity.class);
//            startActivity(i);

//        else if (id == R.id.nav_evaluation) {
//            Intent ic = new Intent(getApplicationContext(),NotifyActivity.class);
//            startActivity(ic);
//
//        } else if (id == R.id.nav_report) {
//            Intent ui = new Intent(getApplicationContext(),UsersPerformanceActivity.class);
//            startActivity(ui);
        }
        else if (id == R.id.nav_profile) {
//            Intent ca = new Intent(getApplicationContext(),SignupActivity.class);
//            startActivity(ca);
        }
        else if (id == R.id.nav_logout) {
//            Intent ic = new Intent(getApplicationContext(),LoginActivity.class);
//            startActivity(ic);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }
}
